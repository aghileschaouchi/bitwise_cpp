#ifndef BITWISE_H
#define BITWISE_H

#include <iostream>
#include <vector>

template<class T>
    class BitType
    {
    private:
        static constexpr unsigned int bits(const unsigned int bytes)
        {
            return (bytes << 3);
        }

        T m_member;
        uint8_t *m_bitSet = nullptr;

    public:
        BitType(T member)
        {
            m_member = member;
            m_bitSet = new uint8_t[bits(sizeof(T))];

            if(!m_bitSet)
                std::cerr << "Allocation failed!" << std::endl;

            for(int i = 0; i < bits(sizeof(T)) ; i++)
            {
                int mask = 1 << i;
                *(m_bitSet+i) = (m_member & mask) ? '1' : '0';
            }
        }

        void printBitSet()
        {
            uint8_t *ptr = m_bitSet + bits(sizeof(T)) - 1;

            for(;ptr != m_bitSet;)
            {
                std::cout << *(ptr--);
            }
            std::cout << *m_bitSet;
//            for(int i = bits(sizeof(T) - 1); i > 2 ; i -= 4)
//            {
//                std::cout << *(m_bitSet+i  );
//                std::cout << *(m_bitSet+i-1);
//                std::cout << *(m_bitSet+i-2);
//                std::cout << *(m_bitSet+i-3) << " " ;
//            }
            std::cout << std::endl;
        }

        T getOpppositeVal()
        {
//            std::bitset<sizeof(T)*8> debuggBitSet(1 << (sizeof(T)*8 - 1));
//            std::cout << "debugg: " << debuggBitSet << std::endl;
//            std::cout << "debugg: " << (m_member & (1 << ((sizeof(T)*8) - 1))) << std::endl;

            if((m_member & (1 << (bits(sizeof(T)) - 1))) == 0)
                return (~m_member + 1);

            return (~(m_member - 1));
        }

        T getMember() const
        {
            return m_member;
        }

        T operator()()
        {
            return m_member;
        }

        template<typename R>
            friend BitType<R> operator&(const BitType<R> &a, const BitType<R> &b);
        template<typename R>
            friend BitType<R> operator|(const BitType<R> &a, const BitType<R> &b);
        template<typename R>
            friend BitType<R> operator^(const BitType<R> &a, const BitType<R> &b);
        template<typename R>
            friend BitType<R> operator<<(const BitType<R> &a, unsigned int shift);
        template<typename R>
            friend BitType<R> operator>>(const BitType<R> &a, unsigned int shift);
        template<typename R>
            friend BitType<R> operator~(const BitType<R> &a);
        template<typename R>
            friend std::ostream& operator<<(std::ostream &os, const BitType<R> &a);
    };

    template<typename R>
        BitType<R> operator&(const BitType<R> &a, const BitType<R> &b)
        {
            return (a.getMember() & b.getMember());
        }
    template<typename R>
        BitType<R> operator|(const BitType<R> &a, const BitType<R> &b)
        {
            return (a.getMember() | b.getMember());
        }
    template<typename R>
        BitType<R> operator^(const BitType<R> &a, const BitType<R> &b)
        {
            return (a.getMember() ^ b.getMember());
        }
    template<typename R>
        BitType<R> operator<<(const BitType<R> &a, unsigned int shift)
        {
            return (a.getMember() << shift);
        }
    template<typename R>
        BitType<R> operator>>(const BitType<R> &a, unsigned int shift)
        {
            return (a.getMember() >> shift);
        }
    template<typename R>
        BitType<R> operator~(const BitType<R> &a)
        {
            return BitType<R>(~(a.getMember()));
        }
    template<typename R>
        std::ostream& operator<<(std::ostream &os, const BitType<R> &a)
        {
            uint8_t *ptr =a.m_bitSet + a.bits(sizeof(R)) - 1;

            for(;ptr != a.m_bitSet;)
            {
                os << *(ptr--);
            }
            os << *a.m_bitSet << std::endl;
            return os;
        }

template<class T>
    class BitOps
    {
    private:
        BitType<T> m_firstMember;
        BitType<T> m_secondMember;
        std::vector<BitType<T>> m_members;

    public:
        BitOps(const std::vector<BitType<T>> &members) : m_members(members) {}
        BitOps(BitType<T> firstMember) : m_firstMember(firstMember) {}
        BitOps(BitType<T> firstMember,BitType<T> secondMember) : m_firstMember(firstMember), m_secondMember(secondMember) {}

        int numOfDiffBits()
        {
            //XOR + Hamming weight
            //https://en.wikipedia.org/wiki/Hamming_weight
        }

        bool isPowerOfTwo()
        {
            return !(m_firstMember() & (m_firstMember() - 1));
        }

        bool isOdd()
        {
//            if((m_firstMember.getMember() & (1 << ((sizeof(T)*8) - 1)) == 0))
            return !(m_firstMember & 1);
//            else
//            {
//                return !(~(m_firstMember.getMember() - 1) & 1);
//            }
        }

        // if (state = 0) : clear a bit; if (state = 0) set a bit
        // x = 0111 0101
        // pos = 2
        // state = 1
        BitType<T> modifyBit(const int pos, const int state)
        {
            return (m_firstMember & ~(1 << pos)) | (-state & (1 << pos));
        }

        BitType<T> setBit(const int pos)
        {
            return (m_firstMember | (1 << pos));
        }

        BitType<T> clearBit(const int pos)
        {
            return (m_firstMember & ~(1 << pos));
        }

        BitType<T> sum()
        {
            return BitType<T>(m_firstMember ^ m_secondMember);
        }

        bool oppositeSigns()
        {
            return (( m_firstMember() ^ m_secondMember()) >> (sizeof(T)*8 - 1));
        }

        template<typename R>
            friend std::ostream& operator<<(std::ostream &os, const BitOps<R> &bo);
    };

template<typename R>
    std::ostream& operator<<(std::ostream &os, const BitOps<R> &bo)
    {
        os << " first member: " << bo.m_firstMember << " | " << bo.m_firstMember << std::endl <<
              "second member: " << bo.m_secondMember << " | " << bo.m_secondMember << std::endl;
        return os;
    }

class Bitwise
{
public:
    Bitwise();

    bool unitTests(int a, int b);
};

#endif // BITWISE_H
