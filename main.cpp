#include <QCoreApplication>

#include "Bitwise.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Bitwise bitwise;
    int b(4), c(3), d(5);
//    bool res = bitwise.detectOppositeSings(c, b);
    bool res2 = bitwise.unitTests(c, d);

    return a.exec();
}
