#include "Bitwise.h"

Bitwise::Bitwise()
{

}

bool Bitwise::unitTests(int a, int b)
{
    BitType<int> _bitType(10000);
    int negVal = _bitType.getOpppositeVal();
    std::cout << "opposite val: " << negVal << std::endl;

    BitOps<int> bitOps(-22, -13);
    std::cout << bitOps;

    BitType<int> AsumB = bitOps.sum();
    std::cout << "a + b: " << AsumB << std::endl;

    (bitOps.oppositeSigns()) ? std::cout << "they have opposite signs" << std::endl : std::cout << "they dont have opposite signs" <<std::endl;

    BitOps<int> testPower(4096, 3);
    (testPower.isPowerOfTwo()) ? std::cout << "power of two" << std::endl : std::cout << "not power of two" <<std::endl;

    std::cout << "testing BytType & BytType: " << std::endl;
    BitType<int> bt1(0x8888);
    BitType<int> bt2(0x9999);
    std::cout << "& :" << (bt1 & bt2) << std::endl;
    std::cout << "| :" << (bt1 | bt2) << std::endl;
    std::cout << "^ :" << (bt1 ^ bt2) << std::endl;
    std::cout << "<< :" << (bt1 << 1) << std::endl;
    std::cout << ">> :" << (bt1 >> 1) << std::endl;
    std::cout << "^ :" << (~bt1) << std::endl;

    std::cout << "-------------" << std::endl;
    BitType<int> bt3(-1);
    BitType<int> bt4(2);
    BitType<int> bt5(4);

    std::cout << bt3;
    std::cout << bt4;
    std::cout << bt5;

    return true;
}
